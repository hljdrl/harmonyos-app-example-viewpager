package org.ohosx.toolbar;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

public final class Toolbar {

    public final  static Toolbar builder(Component component){
        Toolbar toolbar = new Toolbar();
        toolbar.onStart(component);
        return toolbar;
    }
    public final  static Toolbar builder(AbilitySlice abilitySlice){
        Toolbar toolbar = new Toolbar();
        toolbar.onStart(abilitySlice.findComponentById(ResourceTable.Id_app_toolbar));
        return toolbar;
    }
    private Toolbar(){
    }
    private Button back;
    private Button right;
    private Text titleText;
    private Component toolbar;
    public void onStart(Component component){
        if(component!=null ){
            toolbar = component;
            back = (Button) component.findComponentById(ResourceTable.Id_toolbar_back);
            right = (Button) component.findComponentById(ResourceTable.Id_toolbar_right);
            titleText = (Text) component.findComponentById(ResourceTable.Id_toolbar_title);
        }
    }
    public void hideToolbar(){
        if(toolbar!=null){
            toolbar.setVisibility(Component.HIDE);
        }
    }
    public void showToolbar(){
        if(toolbar!=null){
            toolbar.setVisibility(Component.VISIBLE);
        }
    }
    public void setTitle(String title){
        if(titleText!=null){
            titleText.setText(title);
        }
    }
    public void setBackClickedListener(Component.ClickedListener clickedListener){
        if(back!=null){
            back.setClickedListener(clickedListener);
        }
    }
    public void setRightClickedListener(Component.ClickedListener clickedListener){
        if(right!=null){
            right.setClickedListener(clickedListener);
        }
    }
    public void hideBack(){
        if(back!=null){
            back.setVisibility(Component.HIDE);
        }
    }
    public void showBack(){
        if(back!=null){
            back.setVisibility(Component.VISIBLE);
        }
    }
    public void hideRight(){
        if(right!=null){
            right.setVisibility(Component.HIDE);
        }
    }
    public void showRight(){
        if(right!=null){
            right.setVisibility(Component.VISIBLE);
        }
    }
}
