package org.ohosx.viewpager;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;

public abstract class Fragment {

    private String TAG = getClass().getSimpleName();
    private AbilitySlice abilitySlice;

    public Fragment(AbilitySlice abilitySlice) {
        this.abilitySlice = abilitySlice;
    }

    public abstract Component createUIContent(ComponentContainer componentContainer);

    public AbilitySlice getAbilitySlice() {
        return abilitySlice;
    }

    public String getTitle() {
        return TAG;
    }
//    public void onStart(Intent intent) {
//    }
//    public void onActive() {
//    }
//
//    public void onInactive() {
//    }
//
//    public void onForeground(Intent intent) {
//    }
//
//    public void onBackground() {
//    }
//
//    public void onStop() {
//    }
//    public void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
//    }

}
