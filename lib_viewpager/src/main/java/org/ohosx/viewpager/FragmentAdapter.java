package org.ohosx.viewpager;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;

import java.util.ArrayList;
import java.util.List;

public class FragmentAdapter extends PageSliderProvider {

    private List<Fragment> mList = new ArrayList<Fragment>();
    public FragmentAdapter(List<Fragment> list){
        if(list!=null && !list.isEmpty()){
            mList.addAll(list);
        }
    }
    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int i) {
        Fragment itemFragment = mList.get(i);
        Component createView = itemFragment.createUIContent(componentContainer);

//        createView.setLayoutConfig(
//                new StackLayout.LayoutConfig(
//                        ComponentContainer.LayoutConfig.MATCH_PARENT,
//                        ComponentContainer.LayoutConfig.MATCH_PARENT
//                ));

//        Text label = new Text(null);
//        label.setTextAlignment(TextAlignment.CENTER);
//        label.setLayoutConfig(
//                new StackLayout.LayoutConfig(
//                        ComponentContainer.LayoutConfig.MATCH_PARENT,
//                        ComponentContainer.LayoutConfig.MATCH_PARENT
//                ));
//        label.setText(""+i);
//        label.setTextColor(Color.BLACK);
//        label.setTextSize(80);
//        ShapeElement element = new ShapeElement();
//        element.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#AFEEEE")));
//        label.setBackground(element);
////        componentContainer.addComponent(label);
//        createView.setLayoutConfig(
//                new StackLayout.LayoutConfig(
//                        ComponentContainer.LayoutConfig.MATCH_PARENT,
//                        ComponentContainer.LayoutConfig.MATCH_PARENT
//                ));
//        DirectionalLayout layout = new DirectionalLayout(null);
//        layout.addComponent(label);
//        componentContainer.addComponent(createView);
        if(createView.getComponentParent()==null){
            componentContainer.addComponent(createView);
        }
        return createView;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        componentContainer.removeComponent((Component) o);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return false;
    }
}
