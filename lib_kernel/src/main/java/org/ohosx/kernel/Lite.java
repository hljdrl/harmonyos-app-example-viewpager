package org.ohosx.kernel;

import ohos.aafwk.ability.AbilityPackage;
import org.ohosx.kernel.log.Log;

public class Lite {

    public static void install(AbilityPackage abilityPackage, String appTag) {
        Log.install(appTag);
    }
}
