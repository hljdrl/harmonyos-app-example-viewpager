package org.ohosx.kernel.util;

public class StringCompat {


    public static final String string(String... str){
        StringBuffer buf = new StringBuffer();
        if(str!=null){
            for(String item:str){
                buf.append(item);
            }
        }
        return buf.toString();
    }
    public static final String string(Object... str){
        StringBuffer buf = new StringBuffer();
        if(str!=null){
            for(Object item:str){
                buf.append(item.toString());
            }
        }
        return buf.toString();
    }
}
