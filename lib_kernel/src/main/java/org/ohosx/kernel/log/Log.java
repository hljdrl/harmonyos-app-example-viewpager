package org.ohosx.kernel.log;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import org.ohosx.kernel.util.StringCompat;

public class Log {


    // 定义日志标签
    private static HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MY_TAG");
    public static void install(String tag){
        LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, tag);
    }
    public static void i(String tag,String msg){
        HiLog.info(LABEL, StringCompat.string(tag,"->",msg));
    }
    public static void i(String tag,String... msg){
        HiLog.info(LABEL, StringCompat.string(tag,"->",msg));
    }
    public static void d(String tag,String msg){
        HiLog.debug(LABEL, StringCompat.string(tag,msg));
    }
    public static void d(String tag,String... msg){
        HiLog.debug(LABEL, StringCompat.string(tag,msg));

    }



}
