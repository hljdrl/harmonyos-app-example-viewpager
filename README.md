# harmonyos-app-example-viewpager

#### 介绍
harmonyos viewpager 实现tabbar页面


###  碎片页面->第一种实现方式-官方组件--FractionTabAbility范例代码
1.  FractionAbility
2.  FractionManager->FractionScheduler->add|show|replace|remove|hide|show ->submit


###  碎片页面->第一种实现方式-PageSliderAbility范例代码
根据目前官方组件依然未发现有类似android平台Fragment匹配的组件
本范例参考android平台PagerAdapter结构，定义FragmentAdapter数据适配器

<img src="art/viewpager.gif" width="50%"/>

### AbilitySlice路由配置官方文档
1.  https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ability-page-concept-0000000000033573
 虽然一个Page可以包含多个AbilitySlice，但是Page进入前台时界面默认只展示一个AbilitySlice。


### BottomNavigationBar组件来自 
1.   组件来源https://gitee.com/openharmony-tpc/BottomNavigation

### 需要完善
1.  FragmentAdapter生命周期和AbilitySlice同步绑定.
2.  Fragment生命周期与AbilitySlice同步绑定.
3.  BottomNavigationBar组件高度自动适配问题


#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
