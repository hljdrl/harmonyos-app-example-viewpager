package org.ohos.sample.viewpager.fraction;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import org.ohos.sample.viewpager.fraction.slice.TabAbilitySlice;

public class FractionTabAbility extends FractionAbility {

    private static final String KEY_MAIN_ROUTE="__setMainRoute";
    public static final void start(Ability ability,String mainRoute){
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder().withDeviceId("")
                .withBundleName(ability.getBundleName())
                .withAbilityName(FractionTabAbility.class)
                .build();
        intent.setOperation(operation);
        intent.setParam(KEY_MAIN_ROUTE,mainRoute);
        ability.startAbility(intent);
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        String mainRoute = intent.getStringParam(KEY_MAIN_ROUTE);
//        super.setMainRoute(TabAbilitySlice.class.getName());
        super.setMainRoute(mainRoute);

    }
}
