package org.ohos.sample.viewpager.fraction.slice;

import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.ashokvarma.bottomnavigation.BottomNavigationItem;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import org.ohos.sample.viewpager.ResourceTable;
import org.ohos.sample.viewpager.fraction.fn.*;
import org.ohosx.toolbar.Toolbar;

import java.util.ArrayList;
import java.util.List;

public class BottomNavigationBarAbilitySlice extends AbilitySlice  {

    private Toolbar toolbar;
    private FractionAbility fractionAbility;
    private FractionManager fractionManager;
    private List<BaseFraction> fractions = new ArrayList();
    private BottomNavigationBar mBottomNavigationBar;
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_tab_fraction_bottom_navigationbar_v2);
        fractionAbility = (FractionAbility) getAbility();
        fractionManager = fractionAbility.getFractionManager();
        setToolbar();
        setupTabBar();
        setupFraction();
    }

    void setToolbar() {
        toolbar = Toolbar.builder(this);
        toolbar.setTitle("");
        toolbar.setBackClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                terminate();
            }
        });
    }

    void setupFraction() {
        fractions.add(new HomeFraction());
        fractions.add(new SecondFraction());
        fractions.add(new ThirdFraction());
        fractions.add(new MineFraction());
        //
        FractionScheduler scheduler = fractionManager.startFractionScheduler();
        for (BaseFraction fraction : fractions) {
            scheduler.add(ResourceTable.Id_stack_layout, fraction).hide(fraction);
        }
        scheduler.show(fractions.get(0));
        scheduler.submit();
        toolbar.setTitle(fractions.get(0).getTitle());
//        setTabItemSelect(0);
        mBottomNavigationBar.selectTab(0);
    }

    void setupTabBar() {
        mBottomNavigationBar = (BottomNavigationBar)findComponentById(ResourceTable.Id_app_tabbar);
        mBottomNavigationBar.setTabSelectedListener(
                new BottomNavigationBar.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(int position) {
                        setSelectFraction(position);
//                        setTabItemSelect(position);
//                        select.setText("Current select: " + position);
                    }
                    @Override
                    public void onTabUnselected(int position) {}

                    @Override
                    public void onTabReselected(int position) {}
                });
//        mBottomNavigationBar.setBackgroundStyle(BottomNavigationBar.BACKGROUND_STYLE_RIPPLE);
        mBottomNavigationBar.setBarMode(BottomNavigationBar.MODE_FIXED);
        mBottomNavigationBar
                .addItem(
                        new BottomNavigationItem(ResourceTable.Media_ic_home_white_24dp, "Home", getContext())
                                .setActiveColor(0xffF57C00))
                .addItem(
                        new BottomNavigationItem(ResourceTable.Media_ic_book_white_24dp, "Second", getContext())
                                .setActiveColor(0xff00796B))
                .addItem(
                        new BottomNavigationItem(
                                ResourceTable.Media_ic_music_note_white_24dp, "Third", getContext())
                                .setActiveColor(0xff2196F3))
                .addItem(
                        new BottomNavigationItem(ResourceTable.Media_ic_tv_white_24dp, "Mine", getContext())
                                .setActiveColor(0xff8D6E63))
                .setFirstSelectedPosition(0)
                .initialise();

    }

    private void setSelectFraction(int index) {
        if(fractions.isEmpty()){
            return;
        }
        FractionScheduler scheduler = fractionManager.startFractionScheduler();

        scheduler.show(fractions.get(index));
        for (int i = 0; i < fractions.size(); i++) {
            if (i != index) {
                BaseFraction hide = fractions.get(i);
                scheduler.hide(hide);
            }
        }
        scheduler.submit();
        toolbar.setTitle(fractions.get(index).getTitle());
    }

}
