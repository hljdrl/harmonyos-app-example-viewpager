package org.ohos.sample.viewpager.slice;

import ohos.agp.components.Button;
import ohos.agp.components.Component;
import org.ohos.sample.viewpager.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import org.ohos.sample.viewpager.fraction.FractionTabAbility;
import org.ohos.sample.viewpager.fraction.slice.BottomNavigationBarAbilitySlice;
import org.ohos.sample.viewpager.fraction.slice.TabAbilitySlice;
import org.ohos.sample.viewpager.fragment.PageSliderAbility;
import org.ohosx.toolbar.Toolbar;

public class HomeAbilitySlice extends AbilitySlice implements Component.ClickedListener {

    private Toolbar toolbar;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_list);
        Button btn_fraction_ability = (Button) findComponentById(ResourceTable.Id_btn_start_fraction_ability);
        Button btn_viewpager = (Button) findComponentById(ResourceTable.Id_btn_start_viewpager);
        Button btn_start_bottom_navigation = (Button) findComponentById(ResourceTable.Id_btn_start_bottom_navigation);
        btn_fraction_ability.setClickedListener(this);
        btn_viewpager.setClickedListener(this);
        btn_start_bottom_navigation.setClickedListener(this);
        setToolbar();
    }

    private void setToolbar() {
        toolbar = Toolbar.builder(this);
        toolbar.setTitle("Home");
        toolbar.hideBack();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        int id = component.getId();
        if (id == ResourceTable.Id_btn_start_fraction_ability) {
            FractionTabAbility.start(getAbility(), TabAbilitySlice.class.getName());
        } else if (id == ResourceTable.Id_btn_start_viewpager) {
            PageSliderAbility.start(getAbility());
        } else if (id == ResourceTable.Id_btn_start_bottom_navigation) {
            FractionTabAbility.start(getAbility(), BottomNavigationBarAbilitySlice.class.getName());
        }
    }
}
