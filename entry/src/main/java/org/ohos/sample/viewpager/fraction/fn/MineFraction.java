package org.ohos.sample.viewpager.fraction.fn;

import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import org.ohos.sample.viewpager.ResourceTable;

/**
 * 启动fraction
 * 依次调用onComponentAttached、onStart、onActive
 * 按下home键进入后台
 * 依次调用onInactive、onBackground
 * 重新回到前台
 * 依次调用onForeground、onActive
 * 退出fraction
 * 依次调用onInactive、onBackground、onStop、onComponentDetach
 */
public class MineFraction extends BaseFraction {

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component view = scatter.parse(ResourceTable.Layout_fragment_mine, null, false);
        return view;
    }

    @Override
    protected void onComponentDetach() {
        super.onComponentDetach();
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
    }

}
