package org.ohos.sample.viewpager.fragment.slice;

import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.PageSlider;
import org.ohos.sample.viewpager.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import org.ohos.sample.viewpager.fragment.ft.HomeFragment;
import org.ohos.sample.viewpager.fragment.ft.MineFragment;
import org.ohos.sample.viewpager.fragment.ft.SecondFragment;
import org.ohos.sample.viewpager.fragment.ft.ThirdFragment;
import org.ohosx.toolbar.Toolbar;
import org.ohosx.viewpager.Fragment;
import org.ohosx.viewpager.FragmentAdapter;

import java.util.ArrayList;
import java.util.List;

public class PageSliderAbilitySlice extends AbilitySlice implements PageSlider.PageChangedListener, Component.ClickedListener {

    private PageSlider mPageSlider;
    private FragmentAdapter mFragmentAdapter;
    private List<Fragment> fragmentList = new ArrayList<Fragment>();
    private List<DirectionalLayout> tabItems = new ArrayList<DirectionalLayout>();
    private Toolbar toolbar;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_page_slider);
        setToolbar();
        setupTabBar();
        setupFragment();
    }

    private void setToolbar() {
        toolbar = Toolbar.builder(this);
        toolbar.setTitle("");
        toolbar.setBackClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                terminate();
            }
        });
    }

    void setupTabBar() {
        DirectionalLayout tabItem1 = (DirectionalLayout) findComponentById(ResourceTable.Id_item_tab_1);
        DirectionalLayout tabItem2 = (DirectionalLayout) findComponentById(ResourceTable.Id_item_tab_2);
        DirectionalLayout tabItem3 = (DirectionalLayout) findComponentById(ResourceTable.Id_item_tab_3);
        DirectionalLayout tabItem4 = (DirectionalLayout) findComponentById(ResourceTable.Id_item_tab_4);
        tabItems.add(tabItem1);
        tabItems.add(tabItem2);
        tabItems.add(tabItem3);
        tabItems.add(tabItem4);
        //
        tabItem1.setClickedListener(this);
        tabItem2.setClickedListener(this);
        tabItem3.setClickedListener(this);
        tabItem4.setClickedListener(this);
    }

    void setupFragment() {
        fragmentList.add(new HomeFragment(this));
        fragmentList.add(new SecondFragment(this));
        fragmentList.add(new ThirdFragment(this));
        fragmentList.add(new MineFragment(this));
        mFragmentAdapter = new FragmentAdapter(fragmentList);
        mPageSlider = (PageSlider) findComponentById(ResourceTable.Id_page_slider);
        //是否启用页面滑动
        mPageSlider.setSlidingPossible(false);
        //是否启用回弹效果
        mPageSlider.setReboundEffect(false);
        //设置Provider，用于配置PageSlider的数据结构
        mPageSlider.setProvider(mFragmentAdapter);
        //设置要保留当前页面两侧的页面数。
//        mPageSlider.setPageCacheSize(fragmentList.size());

        mPageSlider.addPageChangedListener(this);
        setSelect(mPageSlider.getCurrentPage());
    }

    private void setSelect(int select) {
        tabItems.get(select).setSelected(true);
        toolbar.setTitle(fragmentList.get(select).getTitle());
        for (int i = 0; i < tabItems.size(); i++) {
            if (i != select) {
                DirectionalLayout tabView = tabItems.get(i);
                tabView.setSelected(false);
            }
        }

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onPageSliding(int i, float v, int i1) {

    }

    @Override
    public void onPageSlideStateChanged(int i) {

    }

    @Override
    public void onPageChosen(int i) {
        setSelect(i);
    }

    @Override
    public void onClick(Component component) {
        int index = tabItems.indexOf(component);
        mPageSlider.setCurrentPage(index, false);

    }
}
