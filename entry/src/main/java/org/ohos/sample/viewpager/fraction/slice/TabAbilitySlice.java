package org.ohos.sample.viewpager.fraction.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import org.ohos.sample.viewpager.ResourceTable;
import org.ohos.sample.viewpager.fraction.fn.*;
import org.ohosx.toolbar.Toolbar;

import java.util.ArrayList;
import java.util.List;

public class TabAbilitySlice extends AbilitySlice implements Component.ClickedListener {


    private Toolbar toolbar;
    private List<DirectionalLayout> tabItems = new ArrayList();
    private FractionAbility fractionAbility;
    private FractionManager fractionManager;
    private List<BaseFraction> fractions = new ArrayList();

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_tab_fraction_v2);
        setToolbar();
        setupTabBar();
        setupFraction();
    }

    void setToolbar() {
        toolbar = Toolbar.builder(this);
        toolbar.setTitle("");
        toolbar.setBackClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                terminate();
            }
        });
    }

    void setupFraction() {
        fractionAbility = (FractionAbility) getAbility();
        fractionManager = fractionAbility.getFractionManager();
        //
        fractions.add(new HomeFraction());
        fractions.add(new SecondFraction());
        fractions.add(new ThirdFraction());
        fractions.add(new MineFraction());
        //
        FractionScheduler scheduler = fractionManager.startFractionScheduler();
        for (BaseFraction fraction : fractions) {
            scheduler.add(ResourceTable.Id_stack_layout, fraction).hide(fraction);
        }
        scheduler.show(fractions.get(0));
        scheduler.submit();
        toolbar.setTitle(fractions.get(0).getTitle());
        setTabItemSelect(0);
    }

    void setupTabBar() {
        DirectionalLayout tabItem1 = (DirectionalLayout) findComponentById(ResourceTable.Id_item_tab_1);
        DirectionalLayout tabItem2 = (DirectionalLayout) findComponentById(ResourceTable.Id_item_tab_2);
        DirectionalLayout tabItem3 = (DirectionalLayout) findComponentById(ResourceTable.Id_item_tab_3);
        DirectionalLayout tabItem4 = (DirectionalLayout) findComponentById(ResourceTable.Id_item_tab_4);
        tabItems.add(tabItem1);
        tabItems.add(tabItem2);
        tabItems.add(tabItem3);
        tabItems.add(tabItem4);
        //
        tabItem1.setClickedListener(this);
        tabItem2.setClickedListener(this);
        tabItem3.setClickedListener(this);
        tabItem4.setClickedListener(this);
    }

    private void setSelectFraction(int index) {
        FractionScheduler scheduler = fractionManager.startFractionScheduler();

        scheduler.show(fractions.get(index));
        for (int i = 0; i < fractions.size(); i++) {
            if (i != index) {
                BaseFraction hide = fractions.get(i);
                scheduler.hide(hide);
            }
        }
        scheduler.submit();
        toolbar.setTitle(fractions.get(index).getTitle());
    }

    private void setTabItemSelect(int select) {
        tabItems.get(select).setSelected(true);
        for (int i = 0; i < tabItems.size(); i++) {
            if (i != select) {
                DirectionalLayout tabView = tabItems.get(i);
                tabView.setSelected(false);
            }
        }
    }

    @Override
    public void onClick(Component component) {
        int index = tabItems.indexOf(component);
        setSelectFraction(index);
        setTabItemSelect(index);
    }
}
