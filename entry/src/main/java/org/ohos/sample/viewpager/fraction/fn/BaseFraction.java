package org.ohos.sample.viewpager.fraction.fn;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import org.ohos.sample.viewpager.ResourceTable;
import org.ohosx.kernel.log.Log;

/**
 * 启动fraction
 * 依次调用onComponentAttached、onStart、onActive
 * 按下home键进入后台
 * 依次调用onInactive、onBackground
 * 重新回到前台
 * 依次调用onForeground、onActive
 * 退出fraction
 * 依次调用onInactive、onBackground、onStop、onComponentDetach
 */
public class BaseFraction extends Fraction {

    String TAG = getClass().getSimpleName();

    public String getTitle() {
        return TAG;
    }

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Log.i(TAG, "onComponentAttached");
        return super.onComponentAttached(scatter, container, intent);
    }

    @Override
    protected void onComponentDetach() {
        Log.i(TAG, "onComponentDetach");
        super.onComponentDetach();
    }


    @Override
    protected void onStart(Intent intent) {
        Log.i(TAG, "onStart");
        super.onStart(intent);
    }

    @Override
    protected void onInactive() {
        Log.i(TAG, "onInactive");
        super.onInactive();
    }

    @Override
    protected void onBackground() {
        Log.i(TAG, "onBackground");
        super.onBackground();
    }

    @Override
    protected void onForeground(Intent intent) {
        Log.i(TAG, "onForeground");
        super.onForeground(intent);
    }

    @Override
    protected void onActive() {
        Log.i(TAG, "onActive");
        super.onActive();
    }

    @Override
    protected void onStop() {
        Log.i(TAG, "onStop");
        super.onStop();
    }
}