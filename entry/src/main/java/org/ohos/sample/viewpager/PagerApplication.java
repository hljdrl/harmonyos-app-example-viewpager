package org.ohos.sample.viewpager;

import ohos.aafwk.ability.AbilityPackage;
import org.ohosx.kernel.Lite;

public class PagerApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        Lite.install(this,"viewpager");
    }
}
