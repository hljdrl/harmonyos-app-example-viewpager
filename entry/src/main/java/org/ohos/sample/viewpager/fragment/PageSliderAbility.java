package org.ohos.sample.viewpager.fragment;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import org.ohos.sample.viewpager.fraction.FractionTabAbility;
import org.ohos.sample.viewpager.fragment.slice.PageSliderAbilitySlice;

public class PageSliderAbility extends Ability {

    public static final void start(Ability ability){
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder().withDeviceId("")
                .withBundleName(ability.getBundleName())
                .withAbilityName(PageSliderAbility.class)
                .build();
        intent.setOperation(operation);
        ability.startAbility(intent);
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(PageSliderAbilitySlice.class.getName());
    }
}
