package org.ohos.sample.viewpager.fragment.ft;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import org.ohos.sample.viewpager.ResourceTable;
import org.ohosx.viewpager.Fragment;

public class HomeFragment extends Fragment {


    private DirectionalLayout mView;

    public HomeFragment(AbilitySlice abilitySlice) {
        super(abilitySlice);
    }

    @Override
    public Component createUIContent(ComponentContainer componentContainer) {
        if (mView == null) {
            mView = (DirectionalLayout) LayoutScatter.getInstance(getAbilitySlice().getAbility()).parse(ResourceTable.Layout_fragment_home, null, false);
        }
        return mView;
    }
}
